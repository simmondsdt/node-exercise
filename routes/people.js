const { api } = require('../services');

/**
 * Custom sort function to allow for filtering of api data with unknown format
 * @param items items returned from api
 * @param key key to sort objects by
 * @returns sorted array
 */
const sort = ({ items, key }) => {
  if (key === 'mass' || key === 'height') {
    return items.sort((a, b) => {
      // account for unknown string for values and commas in integers
      const aValue = a[key] === 'unknown' ? 0 : parseFloat(+a[key].replace(/,/g, ''));
      const bValue = b[key] === 'unknown' ? 0 : parseFloat(+b[key].replace(/,/g, ''));
      return aValue > bValue ? 1 : -1;
    });
  } else {
    return items.sort((a, b) => a[key] - b[key]);
  }
};

module.exports = async (req, res) => {
  const { sortBy } = req.query;
  const items = await api({ route: 'people' });
  const data = req.query ? sort({ items, key: sortBy }) : items;
  res.send({ data });
};
