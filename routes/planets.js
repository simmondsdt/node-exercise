const { api } = require('../services');

module.exports = async (req, res) => {
  const apiRes = await api({ route: 'planets', subQuery: { key: 'residents', returnValue: 'name' } });
  res.send({ data: apiRes });
};
