const express = require('express');
const app = express();
const port = 3000;

const routes = require('./routes');

app.get('/people', routes.people);
app.get('/planets', routes.planets);

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
