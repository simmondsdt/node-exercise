const axios = require('axios');

const api = 'https://swapi.dev/api/';

const fetchAll = async ({ items, route, subQuery }) => {
  const res = await axios.get(route);
  const { next, results } = res.data;

  // Current subQuery only allows 1 level deep
  if (subQuery) {
    for (let result of results) {
      if (result[subQuery.key]) {
        result[subQuery.key] = await Promise.all(
          result[subQuery.key].map(async url => {
            const res = await axios.get(url);
            return res.data[subQuery.returnValue];
          })
        );
      }
    }
  }

  const allItems = [...items, ...results];
  return next ? await fetchAll({ items: allItems, route: next, subQuery }) : allItems;
};

module.exports = async ({ route, subQuery }) => {
  const url = `${api}/${route}`;
  try {
    const results = await fetchAll({ items: [], route: url, subQuery });
    return results;
  } catch (error) {
    console.log('error', error);
  }
};
